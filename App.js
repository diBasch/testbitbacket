import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const App = () => {
  const {container} = styles;
  return (
    <View style={container}>
      <Text>test new 2</Text>
    </View>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'grey',
  },
});
